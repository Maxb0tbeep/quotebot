const { SlashCommandBuilder, EmbedBuilder, PermissionsBitField } = require("discord.js");
const dotenv = require("dotenv");
const Quotebook = require("../schemas/QuotebookSchema.js");

dotenv.config();

module.exports = {
  data: new SlashCommandBuilder()
    .setName("setchannel")
    .setDescription("set the channel for quotes to be posted.")
    .addChannelOption((channel) =>
      channel
        .setName("channel")
        .setRequired(true)
        .setDescription("which channel to send quotes in"),
    ),
  async execute(interaction) {
    const qb = await Quotebook.findById(interaction.guild.id).exec();
    const channel = interaction.options.getChannel("channel");

    const successEmbed = new EmbedBuilder()
      .setColor("#3ffc4e")
      .setTitle("Set Channel")
      .setDescription(`:white_check_mark: Channel successfully to <#${channel.id}>`);

    const errorEmbed = new EmbedBuilder()
      .setColor("#fc3c32")
      .setTitle("Set Channel")
      .setDescription(`:x: Something went wrong.`);

    if (channel.type != 0) {
      errorEmbed.setDescription(":x: The quotebook must be set to a text channel");
      await interaction.reply({ embeds: [errorEmbed] });
      return;
    } else if (!interaction.member.permissions.has(PermissionsBitField.Flags.ManageChannels)) {
      errorEmbed.setDescription(":x: You need permission to manage channels to run this command");
      await interaction.reply({ embeds: [errorEmbed] });
      return;
    }

    try {
      if (qb != null) {
        if (qb.channel == channel.id) {
          errorEmbed.setDescription(":x: The quotebook is already set to this channel");
          await interaction.reply({ embeds: [errorEmbed] });
        } else {
          await Quotebook.findOneAndUpdate(
            { _id: interaction.guild.id },
            { $set: { channel: channel.id } },
          ).exec();
          await interaction.reply({ embeds: [successEmbed] });
        }
      } else {
        await Quotebook.create({
          _id: interaction.guild.id,
          channel: channel.id,
          count: 0
        });

        await interaction.reply({ embeds: [successEmbed] });
      }
    } catch (err) {
      errorEmbed.setDescription(`:x: ${err}`);
      await interaction.reply({ embeds: [errorEmbed] });
    }
  },
};
