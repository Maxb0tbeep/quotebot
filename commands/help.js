const { SlashCommandBuilder, EmbedBuilder } = require("discord.js");
const dotenv = require("dotenv");

dotenv.config();

module.exports = {
  data: new SlashCommandBuilder()
    .setName("help")
    .setDescription("How to use Quotebot"),
  async execute(interaction) {
    const helpEmbed = new EmbedBuilder()
      .setColor("#ee5396")
      .setTitle("How to use Quotebot")
      .addFields(
        {
          name: "add quote",
          value: `reply to the message you want to quote by mentioning me. (<@${process.env.CLIENT_ID}>)`,
          inline: true,
        },
        {
          name: "set quotebook channel",
          value: "use the `/setchannel` command",
          inline: true,
        },
      );

    await interaction.reply({ embeds: [helpEmbed] });
  },
};
