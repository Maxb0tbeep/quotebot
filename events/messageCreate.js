const { Events, EmbedBuilder } = require("discord.js");
const dotenv = require("dotenv");
const Quotebook = require("../schemas/QuotebookSchema.js");

dotenv.config();

module.exports = {
  name: Events.MessageCreate,
  async execute(message) {
    const errorEmbed = new EmbedBuilder()
      .setColor("#fc3c32")
      .setTitle("Error")
      .setDescription(":x: Something went wrong");

    if(message.author.bot) {return;}

    if(message.content.includes(`<@${process.env.CLIENT_ID}>`)){
      const qb = await Quotebook.findById(message.guild.id).exec();

      if(message.reference == null) {
        errorEmbed.setDescription(":x: You need to reply to a message to quote it");
        await message.reply({ embeds: [errorEmbed] });
        return;
      }

      try {
        if (qb != null) {
          const channel = message.client.channels.cache.get(message.reference.channelId);
          const quoteChannel = await message.client.channels.fetch(qb.channel);
          const fetchedMessage = await channel.messages.fetch(message.reference.messageId);

          if(fetchedMessage.content == ""){
            errorEmbed.setDescription(":x: Quote must have message content");
            await message.reply({ embeds: [errorEmbed] });
            return;
          }

          await Quotebook.findOneAndUpdate(
            { _id: message.guild.id },
            { $set: { count: qb.count+=1 } },
          ).exec();

          const quoteEmbed = new EmbedBuilder()
            .setColor("#ee5396")
            .setDescription(`"${fetchedMessage}"`)
            .setTitle(`Quotebook Entry #${qb.count}`)
          	.setURL(`https://discord.com/channels/${message.reference.guildId}/${message.reference.channelId}/${message.reference.messageId}`)
          	.setFooter({ text: `${message.mentions.repliedUser.username}, ${new Date(message.createdTimestamp).getFullYear()}`, iconURL: message.mentions.repliedUser.displayAvatarURL() })

          await quoteChannel.send({ embeds: [quoteEmbed] });

          message.react("✅");
        } else {
          errorEmbed.setDescription(":x: The quotebook channel has not been set yet. use `/setchannel`");
          await message.reply({ embeds: [errorEmbed] });
          return;
        }
      } catch(err) {
        errorEmbed.setDescription(`:x: ${err}`);
        await message.reply({ embeds: [errorEmbed] });
        console.log(err);
      }
    }
  },
};
