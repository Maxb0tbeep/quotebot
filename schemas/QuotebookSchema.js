const mongoose = require("mongoose");

const QuotebookSchema = new mongoose.Schema({
  _id: { type: mongoose.SchemaTypes.String, required: true },
  channel: { type: mongoose.SchemaTypes.String, required: true },
  count: { type: mongoose.SchemaTypes.Number, required: true }
});

module.exports = mongoose.model("Quotebook", QuotebookSchema);

